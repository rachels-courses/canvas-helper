# https://canvas.instructure.com/doc/api/assignments.html#method.assignments_api.create
access_token=`cat ../private/access_token.txt`
domain=`cat ../private/domain.txt`
course_id="65930" # Test course
request="assignment[name]=APITest"
curl -H "Authorization: Bearer $access_token" -X POST "$domain/api/v1/courses/$course_id/assignments" -d $request 
