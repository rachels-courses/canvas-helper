# Canvas Helper - API Scripts

## How to generate your Access Token

1. On Canvas, go to your profile then *Settings*.
2. Under *Approved Integrations*, click *+New Access Token* and generate an access token.

## Canvas API Reference

It's here: https://canvas.instructure.com/doc/api/
