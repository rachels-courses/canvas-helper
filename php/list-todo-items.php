<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title> List todo items - R.W.'s utilities </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Wil Sha Singh">
  <link rel="icon" type="image/png" href="http://www.moosadee.com/web-assets/images/favicon-moose.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <script src="sorttable.js"></script> <!-- https://www.kryogenix.org/code/browser/sorttable/ -->
  <style>
    tr:nth-child(odd) {
      background-color: #D1DECF;
      color: #000;
    }
  </style>
</head>
<body>
  

<?
// Sources: https://stackoverflow.com/questions/33302442/get-info-from-external-api-url-using-php

/*
access_token=`cat ../private/access_token.txt`
domain=`cat ../private/domain.txt`
curl -H "Authorization: Bearer $access_token" -X GET "$domain/api/v1/users/self/todo" > ../output/todo-items.json
 * */

if ( isset( $_POST["send_request"] ) )
{
  $path = "/api/v1/users/self/todo?per_page=100";
  
  // header = { "Authorization" : "Bearer " + access_token }
  
  $curl = curl_init();
  curl_setopt_array( $curl, array(
    CURLOPT_URL => $_POST["domain"] . $path,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Authorization: Bearer " . $_POST["access_token"] 
    ),
  ) );
  
  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  
  // Turn the JSON into an array
  // Source: https://www.php.net/manual/en/function.json-decode.php
  $data = json_decode( $response, true );
}

?>

  
<div class="container">

  <h2>Canvas API: List todo / to-grade items</h2>
  <p>Source code: <a href="https://gitlab.com/rachels-courses/canvas-helper/-/blob/main/php/list-todo-items.php?ref_type=heads">https://gitlab.com/rachels-courses/canvas-helper/-/blob/main/php/list-todo-items.php?ref_type=heads</a></p>
  <p>To get an access token: Log into your Canvas page, click on your picture and go to Settings. Under "Approved Integrations" there is a <strong>"+New Access Token"</strong> button.</p>

  <form method="post">
    <p>Access token:<br>
    <input type="text" name="access_token" class="form-control">
    </p>
    <p>Domain:<br>
    <input type="text" name="domain" placeholder="https://canvas.DOMAIN.edu" class="form-control">
    </p>
    <p><input type="submit" name="send_request" value="Get request" class="form-control btn btn-primary"></p>
  </form>
</div>

<hr>

<div class="container-fluid">
  <table class="table sortable">
    <tr>
      <th>Course ID</th>
      <th>Course name</th>
      <th>Assignment</th>
      <th>Assignment link</th>
      <th>Speedgrader link</th>
      <th>Due date</th>
      <th>Lock date</th>
      <th>Needs grading</th>
    </tr>
    <? $total_to_grade = 0; ?>
    <? foreach ( $data as $key => $row ) { ?>
      <tr>
        <td>
          <?=$row["course_id"] ?>
        </td>
        <td>
          <?=$row["context_name"] ?>
        </td>
        <td><?=$row["assignment"]["name"] ?>
        </td>
        <td>
          <a href="<?= $row['assignment']['html_url'] ?>" target="_blank">Assignment</a>
        </td>
        <td>
          <a href="<?= $row['html_url'] ?>" target="_blank">Speedgrader</a>
        </td>
        <td>
          <?
          $due_time = strtotime( $row["assignment"]["due_at"]  );
          $due_date = date( "Y-m-d d H:i:s", $due_time );
          ?>
          <?=$due_date?>
        </td>
        <td>
          <?
          $lock_time = strtotime( $row["assignment"]["lock_at"] );
          $lock_date = date( "Y-m-d d H:i:s", $lock_time );
          ?>
          <?=$lock_date?>
        </td>
        <td>
          <?=$row["needs_grading_count"] ?>
          <? $total_to_grade += $row["needs_grading_count"] ?>
        </td>
      </tr>
    <? } ?> 
  </table>
  <p>Total ASSIGNMENTS to grade: <?=sizeof( $data );?></p>
  <p>Total SUBMISSIONS to grade: <?=$total_to_grade?> </p>
  
    
  <p>Debug:</p>
  <pre style="width:900px; overflow: scroll;">
  <? print_r( $err ); ?>
  </pre>


</div>


</body>
