# https://datagy.io/python-json-to-csv/
import pandas

output_folder = "output/"

infile  = input( "Enter input file: " + output_folder )
outfile = input( "Enter output file: " + output_folder )

data_frame = pandas.read_json( output_folder + infile )
data_frame.to_csv( output_folder + outfile )
