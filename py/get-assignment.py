import requests
import json
import csv

access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()

course_id = "123"
assignment_id = "123"

uri = domain + "/api/v1/courses/" + course_id + "/assignments/" + assignment_id
print( "uri:", uri )
header = { "Authorization" : "Bearer " + access_token }
response = requests.get( uri, headers=header )
json_data = json.loads( response.content )

outpath = "../output/get-assignment_" + course_id + "_" + assignment_id + ".json"
outfile = open( outpath, "w" )
json.dump( json_data, outfile )
print( "Output file:", outpath )
