import requests
import json

# REFERENCE:
# https://canvas.instructure.com/doc/api/rubrics.html#method.rubrics.create
# https://community.canvaslms.com/t5/Canvas-Question-Forum/Creating-a-rubric-using-REST-API/td-p/478191

# Note: This API call will return a 200 OK even if it doesn't create the rubric.
# Note: Before, I used "json.dumps( request_obj )" in the request. Removing that fixed this script and now it seems to work alright.

course_id="65930" # Test course
assignment_id="1405622" # Test assignment
access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()
uri = domain + "/api/v1/courses/" + course_id + "/rubrics"


# Request to add a rubric to a course (for use in multiple assignments)
# request_obj = { 
#   "id":"",
#   "rubric_id":"",
#   "title":"API Rubric2",
#   "points_possible":5,
#   "rubric_association":course_id,
#   "_method":"POST",
#   "authenticity_token":access_token,
#   "rubric":{
#     "title":"API Rubric",
#     "points_possible":5,
#     "free_form_criterion_comments":0,
#     "skip_updating_points_possible":"false",
#     "criteria":{
#       "1":{
#         "id":"itsanid",
#         "description":"Criteria 1",
#         "long_description":"Longboi",
#         "points":5,
#         "learning_outcome_id":"",
#         "criterion_use_range":"false",
#         "ratings":{
#           "1":{
#             "id":"c1r1",
#             "description":"c1r1 description",
#             "long_description":"c1r1 longdesc",
#             "points":5.0
#           },
#           "2":{
#             "id":"c1r2",
#             "description":"c1r2 description",
#             "long_description":"c1r2 longdesc",
#             "points":1.0
#           }
#         }
#       }
#     }
#   },
#   "rubric_association":{
#     "association_type":"Course",
#     "association_id":course_id,
#     "purpose":"bookmark",
#     "use_for_grading":0,
#     "hide_score_total":0,
#     "hide_points":0,
#     "hide_outcome_results":0,
#   }
# }

# Request to add a rubric to an assignment

request_obj = { 
  "id":"",
  "rubric_id":"new",
  "title":"U07EX",
  "points_possible":100,
  "rubric_association_id":"",
  "_method":"POST",
  "authenticity_token":access_token,
  "rubric":{
    "title":"U07EX",
    "points_possible":100,
    "free_form_criterion_comments":0,
    "skip_updating_points_possible":"false",
    "criteria":{ } # set up below
  },
  "rubric_association":{
    "id":"",
    "association_type":"Assignment",
    "association_id":assignment_id,
    "purpose":"grading",
    "use_for_grading":1,
    "hide_score_total":0,
    "hide_points":0,
    "hide_outcome_results":0,
  }
}

index=1
total_programs = 4
for i in range( 1, total_programs+1 ):
  request_obj["rubric"]["criteria"][ str(index) ] = {
    "id":"c" + str( index ),
    "description":"Program " + str(i) + " - Builds",
    "long_description":"Program should build without errors",
    "points":10.0,
    "learning_outcome_id":"",
    "criterion_use_range":"false",
    "ratings":{
      "1":{
        "id":"c" + str(index) + "r1",
        "description":"Program builds",
        "long_description":"At minimum, your program should build and be runnable.",
        "points":10.0
      },
      "2":{
        "id":"c" + str(index) + "r2",
        "description":"Program does not build",
        "long_description":"What good is a program if you can't even run it?",
        "points":0.0
      }
    }
  }
  
  index += 1
  
  request_obj["rubric"]["criteria"][ str(index) ] = {
    "id":"c" + str(index),
    "description":"Program " + str(i) + " - Good UI / Good code",
    "long_description":"Easy to read and understand the running program at a glance",
    "points":5.0,
    "learning_outcome_id":"",
    "criterion_use_range":"false",
    "ratings":{
      "1":{
        "id":"c" + str(index) + "r1",
        "description":"Mastery",
        "long_description":"Good UI and code",
        "points":5.0
      },
      "2":{
        "id":"c" + str(index) + "r2",
        "description":"Satisfactory",
        "long_description":"UI or code could be cleaned up",
        "points":4.5
      },
      "3":{
        "id":"c" + str(index) + "r3",
        "description":"Not yet",
        "long_description":"UI or code cleanliness need work",
        "points":2.0
      },
      "4":{
        "id":"c" + str(index) + "r4",
        "description":"Unassessable",
        "long_description":"Program unavailable or unintelligible",
        "points":0.0
      },
    }
  }
  
  index += 1
  
  request_obj["rubric"]["criteria"][ str(index) ] = {
    "id":"c" + str(index),
    "description":"Program " + str(i) + " - Requirements met",
    "long_description":"Does the program do what it's supposed to?",
    "points":10.0,
    "learning_outcome_id":"",
    "criterion_use_range":"false",
    "ratings":{
      "1":{
        "id":"c" + str(index) + "r1",
        "description":"Mastery",
        "long_description":"All requirements met",
        "points":5.0
      },
      "2":{
        "id":"c" + str(index) + "r2",
        "description":"Satisfactory",
        "long_description":"Requirements mostly met; perhaps minor errors or issues.",
        "points":4.5
      },
      "3":{
        "id":"c" + str(index) + "r3",
        "description":"Not yet",
        "long_description":"Program requirements not met.",
        "points":2.0
      },
      "4":{
        "id":"c" + str(index) + "r4",
        "description":"Unassessable",
        "long_description":"Program is missing or un-usable.",
        "points":0.0
      },
    }
  }
  
  index += 1
    
#print( request_obj["rubric"]["criteria"] )

header = { "Authorization" : "Bearer " + access_token }
response = requests.post( 
  uri, 
  headers=header,
  json=request_obj #json.dumps( request_obj ) # <<<< THE JSON.DUMPS WAS CAUSING THIS TO NOT WORK!!!
)

print( "** CANVAS RETURNS 200 OK EVEN ON FAIL! **" )
print( "** CHECK THE ID - IF IT'S null, THAT MEANS THE REQUEST FAILED. **" )
print()
print( "Status code:", response.status_code )
print( "Reason:", response.reason )
print( "Response:", response )
print( "content:", response.content )



# Get all rubrics
# uri = domain + "/api/v1/courses/" + course_id + "/rubrics?per_page=100"
# header = { "Authorization" : "Bearer " + access_token }
# response = requests.get( uri, headers=header )
# json = json.loads( response.content )

# print( "Response.content:", response.content )
