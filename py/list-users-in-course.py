import requests
import json
import csv

access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()
course_id = "63982"

uri = domain + "/api/v1/courses/" + course_id + "/users?per_page=100"
print( "uri:", uri )
header = { "Authorization" : "Bearer " + access_token }
response = requests.get( uri, headers=header )


print( "Status code:", response.status_code )
print( "Reason:", response.reason )
print( "Response:", response )


if ( str( response.status_code ) != "200" ):
  print( "exit early" )
  exit(1)

print( "content:", response.content )

json_data = json.loads( response.content )

outpath = "../output/list-users.json"
outfile = open( outpath, "w" )
json.dump( json_data, outfile )





outpath = "../output/list-students.csv"

# Generate CSV file, reference: https://datagy.io/python-json-to-csv/
#headers = []
#for row in json:
#    for key in row.keys():
#        if key not in headers:
#            headers.append( key )
#
#csvfile = open( outpath, "w" )
#writer = csv.DictWriter( csvfile, fieldnames=headers )
#writer.writeheader()
#writer.writerows( json )
#
#print( "Output file:", outpath )
