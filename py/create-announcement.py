import requests
import json

# REFERENCE:
# https://canvas.instructure.com/doc/api/discussion_topics.html#method.discussion_topics.create

course_id="65930" # Test course
access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()
uri = domain + "/api/v1/courses/" + course_id + "/discussion_topics"

request_obj = { 
  "_method":"POST",
  "utf8":"✓",
  "authenticity_token":access_token,
  "title":"API Created",
  "message":"Testing 1 2 3",
  "published":"1",
  "delayed_post_at":"",
  "lock_at":"",
  "is_announcement":"1"
}

header = { "Authorization" : "Bearer " + access_token }
response = requests.post( 
  uri, 
  headers=header,
  json=request_obj
)

print( "** CANVAS RETURNS 200 OK EVEN ON FAIL! **" )
print( "** CHECK THE ID - IF IT'S null, THAT MEANS THE REQUEST FAILED. **" )
print()
print( "Status code:", response.status_code )
print( "Reason:", response.reason )
print( "Response:", response )
print( "content:", response.content )

# Example request via web:
#{
#	"_method": "POST",
#	"utf8": "✓",
#	"authenticity_token": "[REDACTED]",
#	"name": "esdgfhjkl/",
#	"context_module[name]": "esdgfhjkl/",
#	"unlock_at": "",
#	"context_module[unlock_at]": "",
#	"requirement_count": "",
#	"context_module[requirement_count]": "",
#	"require_sequential_progress": "0",
#	"context_module[require_sequential_progress]": "0",
#	"context_module[prerequisites]": "",
#	"context_module[completion_requirements][none]": "none"
#}
