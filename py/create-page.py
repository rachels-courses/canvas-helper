import requests
import json

# REFERENCE:
# https://canvas.instructure.com/doc/api/pages.html#method.wiki_pages_api.create

course_id="65930" # Test course
access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()
uri = domain + "/api/v1/courses/" + course_id + "/pages"

request_obj = { 
  "_method":"POST",
  "utf8":"✓",
  "authenticity_token":access_token,
  "wiki_page" : {
    "title":"API CREATED PAGE",
    "body":"<h1>Hello, world!</h1>",
    "editing_roles":"teachers",
    "notify_of_update":"0",
    "published":"0",
    "front_page":"0",
    "publish_at":""
  },
}

header = { "Authorization" : "Bearer " + access_token }
response = requests.post( 
  uri, 
  headers=header,
  json=request_obj
)

print( "** CANVAS RETURNS 200 OK EVEN ON FAIL! **" )
print( "** CHECK THE ID - IF IT'S null, THAT MEANS THE REQUEST FAILED. **" )
print()
print( "Status code:", response.status_code )
print( "Reason:", response.reason )
print( "Response:", response )
print( "content:", response.content )

# Example request via web:
#{
#	"_method": "POST",
#	"utf8": "✓",
#	"authenticity_token": "[REDACTED]",
#	"name": "esdgfhjkl/",
#	"context_module[name]": "esdgfhjkl/",
#	"unlock_at": "",
#	"context_module[unlock_at]": "",
#	"requirement_count": "",
#	"context_module[requirement_count]": "",
#	"require_sequential_progress": "0",
#	"context_module[require_sequential_progress]": "0",
#	"context_module[prerequisites]": "",
#	"context_module[completion_requirements][none]": "none"
#}
