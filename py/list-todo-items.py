import requests
import json
import csv

access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()

uri = domain + "/api/v1/users/self/todo?per_page=100"
header = { "Authorization" : "Bearer " + access_token }
response = requests.get( uri, headers=header )
json = json.loads( response.content )
outfile = "../output/list-todo-items.csv"

# Generate CSV file, reference: https://datagy.io/python-json-to-csv/
headers = json[0].keys()
csvfile = open( outfile, "w" )
writer = csv.DictWriter( csvfile, fieldnames=headers )
writer.writeheader()
writer.writerows( json )
print( "Output file:", outfile )

headers = list( headers )

print( "" )
print( "Headers:", headers )

# generate HTML file with links
headers.remove( "assignment" )
headers.remove( "ignore" )
headers.remove( "ignore_permanently" )
outfile = "../output/list-todo-items.html"
htmlfile = open( outfile, "w" )
htmlfile.write( "<link href='output-style.css' rel='stylesheet'>" )
htmlfile.write( "<table><tr>" )
for header in headers:
    htmlfile.write( "<th>" + header + "</th>" )
htmlfile.write( "</tr>" )
for row in json:
    htmlfile.write( "\n" )
    htmlfile.write( "<tr>" )

    for header in headers:
        if ( header == "html_url" ):
            htmlfile.write( "<td><a href='" + str( row[header] ) + "'>" + str( row[header] ) + "</a></td>" )
        else:
            htmlfile.write( "<td>" + str( row[header] ) + "</td>" )
    
    htmlfile.write( "</tr>" )
htmlfile.write( "\n</table>" )

print( "Output file:", outfile )
