import requests
import json
import csv

access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()

uri = domain + "/api/v1/courses?per_page=100"
print( "uri:", uri )
header = { "Authorization" : "Bearer " + access_token }
response = requests.get( uri, headers=header )
json = json.loads( response.content )
outfile = "../output/list-courses.csv"

# Generate CSV file, reference: https://datagy.io/python-json-to-csv/
headers = []
for row in json:
    for key in row.keys():
        if key not in headers:
            headers.append( key )

csvfile = open( outfile, "w" )
writer = csv.DictWriter( csvfile, fieldnames=headers )
writer.writeheader()
writer.writerows( json )

print( "Output file:", outfile )
