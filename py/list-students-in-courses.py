import requests
import json
import csv

access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()

uri = domain + "/api/v1/courses?per_page=100"
#uri = domain + "/api/v1/courses?per_page=2"
print( "uri:", uri )
header = { "Authorization" : "Bearer " + access_token }
response = requests.get( uri, headers=header )
json_data = json.loads( response.content )

combined_data = []

outpath = "../output/stulist.csv"
course_csv = open( outpath, "w" )
course_csv.write( "date,course,id,name\n" )

# Grab each course's ID, NAME, and START DATE.
for course in json_data:
  
  course_data = { "id" : "?", "start_at" : "?", "name" : "?" }
  for key, val in course.items():
    if ( key == "id" or key == "start_at" or key == "name" ): 
      course_data[ key ] = val
  
  
  course_data[ "students" ] = []
  
  # Grab the users for each course
  print( "Get users in course", course_data["id"], course_data["name"] )
  uri = domain + "/api/v1/courses/" + str( course_data["id"] ) + "/users?per_page=100"
  response = requests.get( uri, headers=header )
  json_data_2 = json.loads( response.content )
  
  for student in json_data_2:
    student_data = {}
    
    print( "student:",student )
    if ( type(student) is str ): continue
    for key, val in student.items():
      if ( key == "id" or key == "sortable_name" or key == "login_id" or key == "email" ): 
        student_data[ key ] = val
    course_data["students"].append( student_data )
    
    
    print( 'course_data["start_at"]', course_data["start_at"] )
    print( 'course_data["name"]', course_data["name"] )
    print( 'student_data[ "id" ]', student_data[ "id" ] )
    print( 'student_data[ "sortable_name" ]', student_data[ "sortable_name" ] )
    course_csv.write( str( course_data["start_at"] ) + "," + course_data["name"] + "," + str(student_data[ "id" ]) + ",\"" + student_data["sortable_name"] + "\"\n" )

  combined_data.append( course_data )  
      
      
      
outpath = "../output/list-students-in-courses.json"
outfile = open( outpath, "w" )
json.dump( combined_data, outfile )


# Generate CSV file, reference: https://datagy.io/python-json-to-csv/
#outpath = "../outpath/list-students-in-courses.csv"
#headers = []
#for row in combined_data:
#  for key in row.keys():
#    if key not in headers:
#      headers.append( key )
#
#csvfile = open( outpath, "w" )
#writer = csv.DictWriter( csvfile, fieldnames=headers )
#writer.writeheader()
#writer.writerows( combined_data )
#
#print( "Output file:", outpath )
