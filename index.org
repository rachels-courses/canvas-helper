# -*- mode: org -*-

#+TITLE: Rachel's Canvas Helper
#+AUTHOR: Rachel Wil Sha Singh

-----

* Webpage view

https://rachels-courses.gitlab.io/canvas-helper/

----

* How to generate your own access token and setup domain

1. On Canvas, go to your profile then *Settings*.
2. Under *Approved Integrations*, click *+New Access Token* and generate an access token.
3. Create a =private= folder and an =access_token.txt= file. Paste your access token in here.
4. Create a =domain.txt= file and paste your Canvas domain here (e.g., =https://canvas.DOMAIN.edu=)

-----

* Utilities

The repository itself (https://gitlab.com/rachels-courses/canvas-helper) contains various helper
scripts, such as using =curl= and Python.

- curl: https://gitlab.com/rachels-courses/canvas-helper/-/tree/main/curl?ref_type=heads
- python: https://gitlab.com/rachels-courses/canvas-helper/-/tree/main/py?ref_type=heads

-----

* Canvas API reference

It's here: https://canvas.instructure.com/doc/api/
