import requests
import json
import csv
import pprint

# REFERENCE:
# https://canvas.instructure.com/doc/api/assignment_groups.html

course_ids=["65930", "56605", "65992"] # Test courses
access_token = open( "../../private/access_token.txt", "r" ).read().strip()
domain = open( "../../private/domain.txt", "r" ).read().strip()
input_path = "assignment-groups.csv"

# Open the CSV file and for each row create a new assignment type
# Reference: https://realpython.com/python-csv/#reading-csv-files-with-csv

def ReadCsv( path ):
	data = []
	headers = []
	
	print( "Opening \"" + path + "\"..." )
	csv_file = open( path )
	csv_reader = csv.reader( csv_file, delimiter=',' )
	
	line_count = 0
	for row in csv_reader:
		if ( line_count == 0 ):
			print( "HEADERS" )
			for col in row:
				print( col )
				headers.append( col )
			
		else:
			print( "DATA" )
			new_data = {}
			for i in range( len( row ) ):
				print( i, headers[i], row[i] )
				new_data[ headers[i] ] = row[i]
				
			data.append( new_data )
		line_count += 1
	
	return data
# end ReadCsv

def CreateAssignmentGroups( data, uri, access_token ):
	for row in data:
		
		icon = ""
		if 		( row["abbreviation"] == "SUP" ): icon = "🏫 "
		elif 	( row["abbreviation"] == "EXE" ): icon = "🏋️ "
		elif 	( row["abbreviation"] == "TEC" ): icon = "🧠 "
		elif 	( row["abbreviation"] == "LAB" ): icon = "💾 "
		elif 	( row["abbreviation"] == "PRO" ): icon = "💻 "
		elif 	( row["abbreviation"] == "MAC" ): icon = "💯 "
		elif 	( row["abbreviation"] == "PT" ): icon = "🇦 "
		elif 	( row["abbreviation"] == "PT" ): icon = "🇿️ "
			
		group_name = icon + row["name"] + " (" + row["abbreviation"] + ")"
		
		request_obj = {
			"_method":"POST",
			"utf8":"✓",
			"authenticity_token":access_token,
			"assignments" : [],
			"group_weight" : row["weight"],
			"name" : group_name
		}
		header = { "Authorization" : "Bearer " + access_token }
		response = requests.post( 
		  uri, 
		  headers=header,
		  json=request_obj
		)
		
		print( response.content )
		
# end CreateAssignmentGroups

print( "DATA: Loading data" )
input_data = ReadCsv( input_path )

print( "API: Creating assignment groups..." )
for course in course_ids:
	uri = domain + "/api/v1/courses/" + course + "/assignment_groups"
	CreateAssignmentGroups( input_data, uri, access_token )
