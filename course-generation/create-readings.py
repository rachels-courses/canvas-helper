import requests
import json
import pandas

# REFERENCE:
# https://canvas.instructure.com/doc/api/assignments.html#method.assignments_api.create
course_ids = {
  "CS200" : "65930", # Template course
  "CS235" : "65992", # Template course
  "CS250" : "56605", # Template course
  "All"   : ""
}

access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()

infile_name = "data-toprep.csv"
input_data = pandas.read_csv( infile_name )

semester = "202401"

for index, row in input_data.iterrows():
  if ( row["Course"] == "All" ): continue
  
  assignment_title = row["Icon"] + " " + row["Unit"] + " " + row["Assignment type name" ] + " - "
  
  if ( str( row["Title"] ) == "" or str( row["Title"] ) == "nan" ): assignment_title += row["Unit name"]
  else: assignment_title += str( row["Title"] )
    
  assignment_title += " (" + semester + "." + row["Unit"] + "." + row["Assignment type short"] + ")"    
  assignment_title += " 🟥🟥🟥" # Mark as "WIP"
  
  course_id = course_ids[ row["Course"] ]
  
  assignment_type = "assignment"
  
  if ( row["Assignment type short"] != "READ" ): continue
  uri = domain + "/api/v1/courses/" + course_id + "/assignments"
  
  print( uri, "...", assignment_title )
  
  request_obj = { 
    "_method":"POST",
    "utf8":"✓",
    "authenticity_token":access_token,
    "assignment" : {
      "name" : assignment_title,
      "grading_type" : "percent",
      "submission_types" : [
        "none"
      ]
    },
  }
  
  header = { "Authorization" : "Bearer " + access_token }
  response = requests.post( 
    uri, 
    headers=header,
    json=request_obj
  )

# print( "** CANVAS RETURNS 200 OK EVEN ON FAIL! **" )
# print( "** CHECK THE ID - IF IT'S null, THAT MEANS THE REQUEST FAILED. **" )
# print()
# print( "Status code:", response.status_code )
# print( "Reason:", response.reason )
# print( "Response:", response )
# print( "content:", response.content )

