import requests
import json
import pandas

# REFERENCE:
# https://canvas.instructure.com/doc/api/modules.html#method.context_modules_api.create
course_ids = {
  "CS200" : "65930", # Template course
  "CS235" : "65992", # Template course
  "CS250" : "56605", # Template course
  "All"   : ""
}

access_token = open( "../private/access_token.txt", "r" ).read().strip()
domain = open( "../private/domain.txt", "r" ).read().strip()

infile_name = "data-toprep.csv"
input_data = pandas.read_csv( infile_name )

modules_done = []

for index, row in input_data.iterrows():
  if ( row["Course"] == "All" ): continue
  
  course_id = course_ids[ row["Course"] ]
  module_name = row["Course"] + "-" + row["Unit"] + ": " + row["Unit name"]
  
  if ( module_name in modules_done ): continue  
  modules_done.append( module_name )
  
  
  uri = domain + "/api/v1/courses/" + course_id + "/modules"

  print( uri, "...", module_name )
  
  request_obj = { 
    "_method":"POST",
    "utf8":"✓",
    "authenticity_token":access_token,
    "module" : {
      "unlock_at":"",
      "requirement_count":"",
      "name" : module_name,
      # "position" : 1,
      "require_sequential_progress":"0",
      "prerequisites":"",
      "completion_requirements":{
        "none":"none"
      }
    },
  }
  
  header = { "Authorization" : "Bearer " + access_token }
  response = requests.post( 
    uri, 
    headers=header,
    json=request_obj
  )

# print( "** CANVAS RETURNS 200 OK EVEN ON FAIL! **" )
# print( "** CHECK THE ID - IF IT'S null, THAT MEANS THE REQUEST FAILED. **" )
# print()
# print( "Status code:", response.status_code )
# print( "Reason:", response.reason )
# print( "Response:", response )
# print( "content:", response.content )

